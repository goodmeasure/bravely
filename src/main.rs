use bravely::archive::{CrowdData, IndexDataReader};
use std::fs;
use std::io::{BufReader, ErrorKind, Write};
use std::path::{Path, PathBuf};

fn main() -> Result<(), ()> {
    let (index, crowd) = (
        BufReader::new(fs::File::open(Path::new("index.fs")).unwrap()),
        BufReader::new(fs::File::open(Path::new("crowd.fs")).unwrap()),
    );

    let mut output_path = PathBuf::from("./output");
    if let Err(err) = fs::create_dir(&output_path) {
        match err.kind() {
            ErrorKind::AlreadyExists => (),
            _ => {
                eprintln!("[ERROR]: {}", err);
                return Err(());
            }
        }
    }

    for btb_file in IndexDataReader::new(index)
        .extract_crowd(crowd)
        .flat_map(CrowdData::try_into_btb)
    {
        output_path.push(&btb_file.filename());
        if let Ok(f) = fs::OpenOptions::new()
            .write(true)
            .create(true)
            .truncate(true)
            .open(&output_path)
        {
            for entry in btb_file {
                let _ = write!(&f, "{}\n{}\n", entry.cmd(), entry.text());
            }
        }
        output_path.pop();
    }

    Ok(())
}
